﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class AddressRepository : IAddressRepository
    {
        private readonly Db db; //for connect DB

        public AddressRepository(Db db)
        {
            this.db = db;
        }
        public bool Address(Address data)
        {
            try
            {
                db.Add(data);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
    }
}
