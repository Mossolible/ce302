﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class DetailRepository : IDetailRepository
    {
        private readonly Db db;//for connect DB

        public DetailRepository(Db db)
        {
            this.db = db;
        }
        public Products Details(Guid ProductsId)
        {
            return db.Products.Where(w => w.ProductsId == ProductsId).FirstOrDefault();
        }
    }
}
