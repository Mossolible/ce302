﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class MainRepository : IMainRepository
    {
        private readonly Db db;//connect db

        public MainRepository(Db db)
        {
            this.db = db;
        }
        public List<MainModel> Main()
        {
            var result = new List<MainModel>();
            var data = db.Products.Select(p => new { p.Type, p.PicturePath, p.ProductName, p.OldPrice, p.NewPrice, p.ProductsId }).Take(4).ToList();

            try
            {
                foreach (var d in data)
                {
                    MainModel may = new MainModel()
                    {
                        ProductsId = d.ProductsId,
                        PicturePath = d.PicturePath,
                        ProductName = d.ProductName,
                        Type = d.Type,
                        OldPrice = d.OldPrice,
                        NewPrice = d.NewPrice
                    };
                    result.Add(may);
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public List<TypeModel> Type()
        {
            var result = new List<TypeModel>();
            var data = db.Products.Select(p => new { p.Type, p.PicturePath }).Take(4).ToList();

            try
            {
                foreach (var d in data)
                {
                    TypeModel res = new TypeModel()
                    {
                        Type = d.Type,
                        PicturePath = d.PicturePath
                    };
                    result.Add(res);
                }
                
                return result;
            }
            catch(Exception ex)
            {
                return result;
            }
        }

    }
}
