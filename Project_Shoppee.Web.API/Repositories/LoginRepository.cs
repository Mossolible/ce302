﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class LoginRepository : ILoginRepository
    {
        private readonly Db db; //for connect DB

        public LoginRepository(Db db)
        {
            this.db = db;
        }
        public Users Login(string Username , string Password)
        {
            return db.Users.Where(w => w.Username == Username
            && w.Password == Password).FirstOrDefault();
        }
    }
}
