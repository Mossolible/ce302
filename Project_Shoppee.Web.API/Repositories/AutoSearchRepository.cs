﻿using Microsoft.EntityFrameworkCore;
using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class AutoSearchRepository: IAutoSearchRepository
    {
        private readonly Db db; //for connect DB

        public AutoSearchRepository(Db db)
        {
            this.db = db;
        }
        public List<string> AutoSearch(string text)
        {
            var data = db.Products.Where(w => w.ProductName.Contains(text)).ToList();
            var result = new List<string>();
            foreach (var r in data)
            {
                result.Add(r.ProductName);
            }

            return result;
        }
    }
}
