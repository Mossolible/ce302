﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public interface IAddressRepository
    {
        bool Address(Address data);
    }
}
