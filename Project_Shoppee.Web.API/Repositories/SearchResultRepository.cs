﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Repositories
{
    public class SearchResultRepository : ISearchResultRepository
    {


        private readonly Db db; //for connect DB

        public SearchResultRepository(Db db)
        {
            this.db = db;
        }
        public List<Products> SearchResult(string text,  string orderby)
        {
            var data = new List<Products>();
            if (orderby == "DESC") //DESC
            {
                data = db.Products.Where(w => w.ProductName.Contains(text)).OrderByDescending(x => x.NewPrice).ToList();
            }
            else //ASC
            {
                data = db.Products.Where(w => w.ProductName.Contains(text)).OrderBy(x => x.NewPrice).ToList();
            }

            return data;
        }
    
    }
}
