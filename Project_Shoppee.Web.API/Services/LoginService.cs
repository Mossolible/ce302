﻿using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class LoginService :ILoginService
    {
        private readonly ILoginRepository loginRepository; // for connect to repository

        public LoginService(ILoginRepository loginRepository)
        {
            this.loginRepository = loginRepository;
        }
        public Users Login(string Username , string Password)
        {
            var result = new Users();

            result = loginRepository.Login(Username, Password);

            return result;
        }
    }
}
