﻿using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class AddressService : IAddressService
    {
        private readonly IAddressRepository addressRepository; // for connect to repository

        public AddressService(IAddressRepository addressRepository)
        {
            this.addressRepository = addressRepository;
        }
        public bool Address(Address data)
        {
            return addressRepository.Address(data);
        }
    }
}   
