﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public interface IAutoSearchService
    {
        List<string> AutoSearch(string text);
    }
}
