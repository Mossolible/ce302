﻿using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class SearchResultService : ISearchResultService
    {
        private readonly ISearchResultRepository searchResultRepository; // for connect to repository

        public SearchResultService(ISearchResultRepository searchResultRepository)
        {
            this.searchResultRepository = searchResultRepository;
        }
        public List<Products> SearchResult(string text, string orderby)
        {
            var result = new List<Products>();

            result = searchResultRepository.SearchResult(text, orderby);

            return result;
        }
    }
}
