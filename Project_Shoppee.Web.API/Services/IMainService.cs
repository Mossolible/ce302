﻿using Project_Shoppee.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public interface IMainService
    {
        List<MainModel> Main();
        List<TypeModel> Type();
    }
}
