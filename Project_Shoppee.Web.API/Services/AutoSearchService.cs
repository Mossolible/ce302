﻿using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class AutoSearchService : IAutoSearchService
    {
        private readonly IAutoSearchRepository autoSearchRepository; // for connect to repository

        public AutoSearchService(IAutoSearchRepository autoSearchRepository)
        {
            this.autoSearchRepository = autoSearchRepository;
        }
        public List<string> AutoSearch(string text)
        {
            var result = new List<string>();

            result = autoSearchRepository.AutoSearch(text);

            return result;
        }
    }
}
