﻿using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class MainService : IMainService
    {
        private readonly IMainRepository mainRepository;// for Repo

        public MainService(IMainRepository mainRepository)
        {
            this.mainRepository = mainRepository;
        }
        public List<MainModel> Main()
        {
            var result = new List<MainModel>();

            result = mainRepository.Main();

            return result;
        }
        public List<TypeModel> Type()
        {
            var result = new List<TypeModel>();

            result = mainRepository.Type();

            return result;
        }
    }
}
