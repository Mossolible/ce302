﻿using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Services
{
    public class DetailService : IDetailService
    {
        private readonly IDetailRepository detailRepository; // for connect to IDetail

        public DetailService(IDetailRepository detailRepository)
        {
            this.detailRepository = detailRepository;
        }
        public Products Details(Guid ProductsId)
        {
            var result = new Products();

            result = detailRepository.Details(ProductsId);

            return result;
        }
    }
}
