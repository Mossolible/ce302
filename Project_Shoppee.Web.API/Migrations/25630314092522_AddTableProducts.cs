﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Project_Shoppee.Web.API.Migrations
{
    public partial class AddTableProducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductsId = table.Column<Guid>(nullable: false),
                    PicturePath = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    OldPrice = table.Column<int>(nullable: false),
                    NewPrice = table.Column<int>(nullable: false),
                    Rate = table.Column<double>(nullable: false),
                    SelledCount = table.Column<int>(nullable: false),
                    DeliveryPrice = table.Column<int>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    ProductDetial = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductsId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
