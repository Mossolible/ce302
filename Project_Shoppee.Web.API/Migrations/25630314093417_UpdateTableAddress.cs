﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Project_Shoppee.Web.API.Migrations
{
    public partial class UpdateTableAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Address_Users_UsersId1",
                table: "Address");

            migrationBuilder.DropIndex(
                name: "IX_Address_UsersId1",
                table: "Address");

            migrationBuilder.DropColumn(
                name: "UsersId1",
                table: "Address");

            migrationBuilder.AddColumn<Guid>(
                name: "UsersId",
                table: "Address",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Address_UsersId",
                table: "Address",
                column: "UsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_Address_Users_UsersId",
                table: "Address",
                column: "UsersId",
                principalTable: "Users",
                principalColumn: "UsersId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Address_Users_UsersId",
                table: "Address");

            migrationBuilder.DropIndex(
                name: "IX_Address_UsersId",
                table: "Address");

            migrationBuilder.DropColumn(
                name: "UsersId",
                table: "Address");

            migrationBuilder.AddColumn<Guid>(
                name: "UsersId1",
                table: "Address",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Address_UsersId1",
                table: "Address",
                column: "UsersId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Address_Users_UsersId1",
                table: "Address",
                column: "UsersId1",
                principalTable: "Users",
                principalColumn: "UsersId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
