﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetailsController : ControllerBase
    {
        private readonly IDetailService detailService;

        public DetailsController(IDetailService detailService)
        {
            this.detailService = detailService;
        }

        [HttpPost]
        public IActionResult Details([FromBody] Products model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = detailService.Details(model.ProductsId);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}