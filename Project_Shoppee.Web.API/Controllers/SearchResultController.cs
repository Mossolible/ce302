﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchResultController : ControllerBase
    {
        private readonly ISearchResultService searchResultService;

        public SearchResultController(ISearchResultService searchResultService)
        {
            this.searchResultService = searchResultService;
        }
        [HttpPost]
        public IActionResult SearchResult([FromBody] SearchResultModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = searchResultService.SearchResult(model.text, model.orderby);
            if (result.Count != 0)
            {
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}