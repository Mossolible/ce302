﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly IAddressService addressService;

        public AddressController(IAddressService addressService)
        {
            this.addressService = addressService;
        }
        [HttpPost]
        public IActionResult Address([FromBody] Address model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = addressService.Address(model);
            if (result)
            {
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}