﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API.Controllers

{
    [Route("api/[controller]")]
    [ApiController]
    public class AutoSearchController : Controller
    {
        private readonly IAutoSearchService autoSearchService;

        public AutoSearchController(IAutoSearchService autoSearchService)
        {
            this.autoSearchService = autoSearchService;
        }
        [HttpPost]
        public IActionResult AutoSearch([FromBody] AutoSearchModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = autoSearchService.AutoSearch(model.text) ;
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}