﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainController : ControllerBase
    {
        private readonly IMainService mainService;

        public MainController (IMainService mainService)
        {
            this.mainService = mainService;
        }

        [HttpGet]
        public IActionResult Main()
        {
            var result2 = mainService.Type();
            var result1 = mainService.Main();
            if (result1.Count != 0 && result2.Count != 0)
            {
                MainResult result = new MainResult()
                {
                    mainModel = result1,
                    typeModel = result2
                };
                return Ok(result);
            }
            else
            {
                return BadRequest();
            }
            
        }
    }
}