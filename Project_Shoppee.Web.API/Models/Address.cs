﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class Address
    {
        public Guid AddressId { get; set; }
        public Users Users { get; set; }
        public Guid UsersId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string TelNo { get; set; }
        public string Addresses { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string PostNo { get; set; }
    }
}
