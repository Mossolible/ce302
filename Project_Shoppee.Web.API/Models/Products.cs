﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class Products
    {
        public Guid ProductsId { get; set; }
        public string PicturePath { get; set; }
        public string ProductName { get; set; }
        public string Type { get; set; }
        public int OldPrice { get; set; }
        public int NewPrice { get; set; }
        public double Rate { get; set; }
        public int SelledCount { get; set; }
        public int DeliveryPrice { get; set; }
        public string Location { get; set; }
        public string ProductDetial { get; set; }
    }
}
