﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class MainModel
    {
        public Guid ProductsId { get; set; }
        public string PicturePath { get; set; }
        public string ProductName { get; set; }
        public string Type { get; set; }
        public int OldPrice { get; set; }
        public int NewPrice { get; set; }
    }

    public class MainResult
    {
        public List<MainModel> mainModel { get; set; }
        public List<TypeModel> typeModel { get; set; }
    }
}
