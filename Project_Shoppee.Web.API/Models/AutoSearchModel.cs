﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class AutoSearchModel
    {
        [Required]
        public string text { get; set; }
    }
}
