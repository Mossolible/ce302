﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class TypeModel
    {
        public string PicturePath { get; set; }
        public string Type { get; set; }
    }
}
