﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Shoppee.Web.API.Models
{
    public class SearchResultModel
    {

        [Required]
        public string text { get; set; }
        [Required]
        public string orderby { get; set; }
    }
}
