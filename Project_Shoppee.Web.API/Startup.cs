using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Project_Shoppee.Web.API.Models;
using Project_Shoppee.Web.API.Repositories;
using Project_Shoppee.Web.API.Services;

namespace Project_Shoppee.Web.API
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<Db>(option =>option.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Initial Catalog=Shopee;Persist Security Info=False;"));

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();

            //services
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IMainService, MainService>();
            services.AddScoped<IAutoSearchService, AutoSearchService>();
            services.AddScoped<IDetailService, DetailService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<ISearchResultService, SearchResultService>();


            //repositories
            services.AddScoped<ILoginRepository, LoginRepository>();
            services.AddScoped<IMainRepository, MainRepository>();
            services.AddScoped<IAutoSearchRepository, AutoSearchRepository>();
            services.AddScoped<IDetailRepository, DetailRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<ISearchResultRepository, SearchResultRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
